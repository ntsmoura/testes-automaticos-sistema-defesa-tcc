import datetime
import json
import os.path
import random
import unittest
from typing import Tuple

import requests

NOME_TCC = "TEST_ENG2_EQUIPE_NATAN"

user_id, token = None, None


def carregar_banca():
    with open("banca_teste.json", "r") as file:
        return json.load(file)


class ApiTests(unittest.TestCase):
    def login(self, username: str, password: str) -> Tuple[int, str, str]:
        """
        Faz login na API.

        Parameters:

            username (str): username do usuário.
            password (str): password do usuário.

        Returns:

            Uma tupla contendo, respectivamente, um inteiro (id do usuário), uma string (token do usuário) e outra string (nome do usuário)
        """
        with requests.post(
            "https://sistema-de-defesa.herokuapp.com/login",
            data={"username": username, "password": password},
        ) as response:
            response.raise_for_status()
            response_json = response.json()
            user_id = response_json["id"]
            token = response_json["token"]
            name = response_json["nome"]
        return user_id, token, name

    def cadastrar_banca(
        self,
        titulo,
        resumo,
        abstract,
        autor,
        matricula,
        palavras_chave,
        turma,
        curso,
        tipo_banca,
        ano,
        semestre_letivo,
        data_realizacao,
        hora,
        disciplina,
        local,
    ) -> dict:
        """
        Cadastra banca para defesa de TCC.

        Parameters:

            titulo : titulo do TCC
            resumo: resumo do TCC
            abstract: abstract do TCC
            autor: autor do TCC
            matricula: matricula do autor do TCC
            palavras_chave: palavras chave do TCC
            turma: turma do autor do TCC
            curso: curso do autor do TCC
            tipo_banca: tipo da banca
            ano: ano da banca
            semestre_letivo: semestre letivo da banca
            data_realizacao: data da banca
            hora: horário da banca
            disciplina: código da disciplina correpsondente
            local: local da apresentação, opcional
            link: link da apresentação, opcional

        Returns:

            Dicionário com resposta da requisição.
        """

        payload = {
            "titulo_trabalho": titulo,
            "resumo": resumo,
            "abstract": abstract,
            "autor": autor,
            "matricula": matricula,
            "palavras_chave": palavras_chave,
            "turma": turma,
            "curso": curso,
            "tipo_banca": tipo_banca,
            "ano": ano,
            "semestre_letivo": semestre_letivo,
            "data_realizacao": data_realizacao,
            "hora": hora,
            "disciplina": disciplina,
            "local": local,
        }

        user_id, token, _ = self.login("admin", "admin")
        payload["user_id"] = user_id

        with requests.post(
            "https://sistema-de-defesa.herokuapp.com/banca",
            data=payload,
            headers={"Authorization": token},
        ) as response:
            response.raise_for_status()
            response_json = response.json()

        return response_json

    def carregar_banca(self) -> dict:
        """
        Retorna dicionário da banca cadastrada pelos testes.

        Returns:

           Dicionário contendo última banca cadastrada pelos testes.
        """
        with open("banca_teste.json", "r") as file:
            return json.load(file)

    def test_listagem_bancas(self):
        with requests.get("https://sistema-de-defesa.herokuapp.com/banca") as response:
            response.raise_for_status()
            response_json = response.json()

        self.assertEqual(response_json["message"], "Success Message")
        self.assertIsNotNone(response_json["data"])

    def test_login_success(self):
        global user_id
        global token

        user_id, token, name = self.login("admin", "admin")

        self.assertEqual(user_id, 23)
        self.assertEqual(name, "Admin de Oliveira Orientador Santos")
        self.assertIsNotNone(token)

    def test_login_fail(self):
        try:
            _, _, _ = self.login("admin", "admin*")
        except requests.exceptions.HTTPError as error:
            self.assertEqual(error.response.status_code, 403)

    def test_cadastrar_banca(self):
        if not os.path.exists("banca_teste.json"):
            user_id, token, _ = self.login("admin", "admin")

            response = self.cadastrar_banca(
                NOME_TCC,
                "TEXTO",
                "TEXTO",
                "NATAN",
                219116035,
                "Nat, Moura",
                "010101",
                "BCC",
                "local",
                2018,
                2,
                datetime.datetime.now() + datetime.timedelta(days=1),
                datetime.datetime.now(),
                "MATA67",
                "PAF1",
            )

            self.assertEqual(response["message"], "Success Message")

            with requests.get(
                f"https://sistema-de-defesa.herokuapp.com/banca/{user_id}/bancas",
                headers={"Authorization": token},
            ) as response:
                response.raise_for_status()
                response_json = response.json()

            banca_cadastrada = None
            for banca in response_json["data"]:
                if banca["titulo_trabalho"] == NOME_TCC:
                    banca_cadastrada = banca
                    break

            self.assertIsNotNone(banca_cadastrada)

            with open("banca_teste.json", "w") as file:
                json.dump(banca_cadastrada, file)

    def test_minhas_defesas_success(self):
        if not os.path.exists("banca_teste.json"):
            self.test_cadastrar_banca()

        global user_id
        global token
        if user_id is None or token is None:
            self.test_login_success()

        with requests.get(
            f"https://sistema-de-defesa.herokuapp.com/banca/{user_id}/bancas",
            headers={"Authorization": token},
        ) as response:
            response.raise_for_status()
            response_json = response.json()

        self.assertEqual(response_json["message"], "Success Message")

        bancas = response_json["data"]

        self.assertGreaterEqual(len(bancas), 1)

        with open("banca_teste.json", encoding="UTF-8") as file:
            item = json.load(file)

        self.assertIn(item, bancas)

    def test_minhas_defesas_auth_fail(self):
        global user_id
        global token
        if user_id is None or token is None:
            self.test_login_success()
        try:
            with requests.get(
                f"https://sistema-de-defesa.herokuapp.com/banca/{user_id}/bancas",
                headers={"Authorization": None},
            ) as response:
                response.raise_for_status()
        except requests.exceptions.HTTPError as error:
            self.assertEqual(error.response.status_code, 403)

    def test_defesas_que_participa_success(self):
        if not os.path.exists("banca_teste.json"):
            self.test_cadastrar_banca()

        global user_id
        global token
        if user_id is None or token is None:
            self.test_login_success()

        with requests.get(
            f"https://sistema-de-defesa.herokuapp.com/usuario/{user_id}/banca",
            headers={"Authorization": token},
        ) as response:
            response.raise_for_status()
            response_json = response.json()

        self.assertEqual(response_json["message"], "Success Message")

        bancas = response_json["data"]

        self.assertGreaterEqual(len(bancas), 1)

        with open("banca_teste.json", encoding="UTF-8") as file:
            item = json.load(file)

        self.assertIn(item, bancas)

    def test_defesas_que_participa_auth_fail(self):
        global user_id
        global token
        if user_id is None or token is None:
            self.test_login_success()

        try:
            with requests.get(
                f"https://sistema-de-defesa.herokuapp.com/usuario/{user_id}/banca",
                headers={"Authorization": None},
            ) as response:
                response.raise_for_status()
        except requests.exceptions.HTTPError as error:
            self.assertEqual(error.response.status_code, 403)

    def test_defesas_que_participa_negative_random_user_id(self):
        global user_id
        global token
        if user_id is None or token is None:
            self.test_login_success()
        random_negative_user_id = random.randint(-2147483647, -1)

        with requests.get(
            f"https://sistema-de-defesa.herokuapp.com/usuario/{random_negative_user_id}/banca",
            headers={"Authorization": token},
        ) as response:
            response.raise_for_status()
            response_json = response.json()
        self.assertEqual(response_json["message"], "Success Message")

        bancas = response_json["data"]

        self.assertEqual(len(bancas), 0)

    def test_editar_banca(self):
        if not os.path.exists("banca_teste.json"):
            self.test_cadastrar_banca()

        global user_id
        global token
        if user_id is None or token is None:
            self.test_login_success()

        banca = carregar_banca()

        banca["turma"] = "070707"

        with requests.put(
            f"https://sistema-de-defesa.herokuapp.com/banca/{banca['id']}",
            headers={"Authorization": token},
            data=banca,
        ) as response:
            response.raise_for_status()
            response_json = response.json()

        self.assertEqual(response_json["message"], "Success Message")

        result = response_json["data"]

        if isinstance(result["user_id"], str):
            result["user_id"] = int(result["user_id"])

        self.assertEqual(result["turma"], "070707")

        with open("banca_teste.json", "w") as file:
            json.dump(result, file)

    def test_adicionar_nota(self):
        if not os.path.exists("banca_teste.json"):
            self.test_cadastrar_banca()

        global user_id
        global token
        if user_id is None or token is None:
            self.test_login_success()

        banca = carregar_banca()

        with requests.get(
            f"https://sistema-de-defesa.herokuapp.com/usuario-banca/usuarios/{banca['id']}",
            headers={"Authorization": token},
            data=banca,
        ) as response:
            response.raise_for_status()
            response_json = response.json()

        self.assertEqual(response_json["message"], "Success Message")

        result = response_json["data"]

        self.assertGreaterEqual(len(result), 1)

        for usuario in result:
            nota = 0.0
            if usuario["role"] == "orientador":
                nota = 7.0
            elif usuario["role"] == "avaliador":
                nota = 9.0
            with requests.post(
                f"https://sistema-de-defesa.herokuapp."
                f"com/usuario-banca/nota/{banca['id']}/{usuario['id']}",
                data={"avaliador": usuario["id"], "modalOwner": True, "nota": nota},
                headers={"Authorization": token},
            ) as response:
                response.raise_for_status()
                response_json = response.json()

            self.assertEqual(response_json["message"], "Success Message")
            self.assertEqual(response_json["data"], "Nota atualizada com sucesso")

    def test_baixar_pdf(self):
        if not os.path.exists("banca_teste.json"):
            self.test_cadastrar_banca()

        global user_id
        global token
        if user_id is None or token is None:
            self.test_login_success()

        banca_relat_id = carregar_banca()["id"]

        if banca_relat_id is not None:
            with requests.get(
                f"https://sistema-de-defesa.herokuapp.com/documento/documentoInfo/{banca_relat_id}",
                headers={"Authorization": token},
            ) as response:
                response.raise_for_status()
                response_json = response.json()

            self.assertEqual(response_json["titulo_trabalho"], "TEST_ENG2_EQUIPE_NATAN")
            response_json["avaliadores"] = json.dumps(response_json["avaliadores"])
            with requests.post(
                f"https://sistema-de-defesa.herokuapp.com/documento/{banca_relat_id}",
                headers={"Authorization": token},
                data=response_json,
            ) as response:
                response.raise_for_status()
                self.assertEqual(response.headers["Content-Type"], "application/pdf")
                self.assertEqual(response.status_code, 200)
                pdf = open(f"{NOME_TCC}.pdf", "wb")
                pdf.write(response.content)
                pdf.close()

    def test_deletar_banca(self):
        if not os.path.exists("banca_teste.json"):
            self.test_cadastrar_banca()

        global user_id
        global token
        if user_id is None or token is None:
            self.test_login_success()

        banca = carregar_banca()

        with requests.delete(
            f"https://sistema-de-defesa.herokuapp.com/banca/{banca['id']}/delete",
            headers={"Authorization": token},
        ) as response:
            response.raise_for_status()
            self.assertEqual(response.status_code, 204)

        os.remove("banca_teste.json")


if __name__ == "__main__":
    unittest.main()
